CREATE TABLE seances(
  id INT AUTO_INCREMENT PRIMARY KEY,
  titre VARCHAR(255),
  description TEXT,
  heureDebut TIME NOT NULL,
  date DATE NOT NULL,
  duree INT NOT NULL,
  nbParticipantsMax INT NOT NULL,
  couleur VARCHAR(255)
);

CREATE TABLE users(
  id INT AUTO_INCREMENT PRIMARY KEY,
  nom VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  isAdmin TINYINT,
  isActif TINYINT,
  token VARCHAR(255) CHARACTER SET utf-8 COLLATE utf8_general_ci NOT NULL
);

CREATE TABLE inscrits (
  id_seance INT,
  id_user INT,
  PRIMARY KEY(id_seance, id_user),
  FOREIGN KEY (id_seance) REFERENCES seances(id),
  FOREIGN KEY (id_user) REFERENCES users(id)
);
