<?php
use PHPUnit/Framework/TestCase;

include_onde(__DIR__."/../models/Seance.php");
include_onde(__DIR__."/../models/User.php");
include_onde(__DIR__."/../models/Database.php");

final class SeanceTest extends TestCase{

  public function testCreateSeance(){
    $seance = Seance::createSeance("Pilats", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");

    $database = new Database();

    $this->assertNotFalse($database->createSeance($seance));


  }
}



?>
