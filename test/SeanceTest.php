<?php
use PHPUnit\Framework\TestCase;

include_once(__DIR__."/../models/Seance.php");
include_once(__DIR__."/../models/User.php");
include_once(__DIR__."/../models/Database.php");

final class SeanceTest extends TestCase{

  public function testCreateSeance(){
    $database = new Database();
    $seance = Seance::createSeance("Pilats", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");

    $this->assertNotFalse($database->createSeance($seance));
  }


  public function testGetSeanceById(){
    $database = new Database();
    $seance = Seance::createSeance("PilatEs", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");

    $id = $database->createSeance($seance);

    $this->assertInstanceOf(Seance::class, $database->getSeanceById($id));

  }


  public function testGetSeanceByWeek(){
    $database = new Database();
    $seance = Seance::createSeance("PilatON", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");

    $this->assertNotFalse($database->createSeance($seance));

    $nbSeances = count($database->getSeanceByWeek(date("W")));
    echo($nbSeances);

    $this->assertGreaterThan(0, $nbSeances);
  }

  public function testUpdateSeance(){
    $database = new Database();
    $seance = Seance::createSeance("Pilates", "Ce cours excite", "09:00", date("Y-m-d"), 50, 20, "#71d3f6");

    $id = $database->createSeance($seance);
    $seance = $database->getSeanceById($id);


    $this->assertInstanceOf(Seance::class, $seance);

    $seance->setTitre("Tir à l'arc");
    $this->assertTrue($database->updateSeance($seance));
  }

  public function testGetSeanceByUserId() {
    $database = new Database();
    $user = User::createUser("CALAS", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));

    $idUser = $database->createUser($user);
    $this->assertNotFalse($idUser);
    $this->assertEquals(0, count($database->getSeanceByUserId($idUser)));

    $seance1 = Seance::createSeance("Pilates", "Ce cours excite", "09:00", date("Y-m-d"), 50, 20, "#71d3f6");
    $idSeance1 = $database->createSeance($seance1);
    $this->assertNotFalse($idSeance1);
    $this->assertTrue($database->insertParticipant($idUser, $idSeance1));
    $this->assertEquals(1, count($database->getSeanceByUserId($idUser)));


    $seance2 = Seance::createSeance("Yoga", "Ce cours cacète", "10:00", date("Y-m-d"), 50, 20, "#71d3f6");
    $idSeance2 = $database->createSeance($seance2);
    $this->assertNotFalse($idSeance2);
    $this->assertTrue($database->insertParticipant($idUser, $idSeance2));
    $this->assertEquals(2, count($database->getSeanceByUserId($idUser)));

  }


  public function testInsertDeleteParticipant(){
    $database = new Database();

    $user = User::createUser("Sarah", "igore@gmail.com", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));
    $idUser = $database->createUser($user);
    $this->assertNotFalse($idUser);

    $seance = Seance::createSeance("Instinctif", "Ce cours excite", "09:00", date("Y-m-d"), 50, 20, "#71d3f6");
    $idSeance = $database->createSeance($seance);
    $this->assertNotFalse($idSeance);

    $this->assertTrue($database->insertParticipant($idUser, $idSeance));
    $this->assertTrue($database->deleteParticipant($idUser, $idSeance));
  }


  public function testDeleteSeance(){
    $database = new Database();
    $seance = Seance::createSeance("Instinctif", "Ce cours excite", "09:00", date("Y-m-d"), 50, 20, "#71d3f6");

    $id = $database->createSeance($seance);

    $this->assertTrue($database->deleteSeance($id));
  }




  public static function tearDownAfterClass(){
    $database = new Database();
    $database->deleteAllInscrits();
    $database->deleteAllUsers();
    $database->deleteAllSeances();
  }
}
?>
