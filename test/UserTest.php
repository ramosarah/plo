<?php
use PHPUnit\Framework\TestCase;

include_once(__DIR__."/../models/Seance.php");
include_once(__DIR__."/../models/User.php");
include_once(__DIR__."/../models/Database.php");


final class UserTest extends TestCase{

  public function testCreateUser(){
    $database = new Database();

    $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));

    $this->assertNotFalse($database->createUser($user));
  }


  public function testGetAndActivateUser(){
    $database = new Database();

    $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));
    $idUser = $database->createUser($user);
    $this->assertNotFalse($idUser);

    $this->assertTrue($database->activateUser($idUser));

    $user = $database->getUserById($idUser);
    $this->assertInstanceOf(User::class, $user);

    $this->assertEquals(1, $user->getIsActif());
  }


  public function testIsEmailExists() {
    $database = new Database();

    $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));
    $this->assertNotFalse($database->createUser($user));

    $emailTrue = "toto@gmail.com";
    $this->assertTrue($database->isEmailExists($emailTrue));

    $emailFalse = "toto@hotmail.com";
    $this->assertFalse($database->isEmailExists($emailFalse));
  }

  public function testGetUserByEmail() {
    $database = new Database();

    $user = User::createUser("Lolo", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));
    $this->assertNotFalse($database->createUser($user));

    $emailTrue = "toto@gmail.com";
    $this->assertTrue($database->isEmailExists($emailTrue));

    $emailFalse = "toto@hotmail.com";
    $this->assertFalse($database->isEmailExists($emailFalse));
  }




  public static function tearDownAfterClass(){
    $database = new Database();
    $database->deleteAllInscrits();
    $database->deleteAllUsers();
    $database->deleteAllSeances();
  }
}
?>
