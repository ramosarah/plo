<?php
class User {

  private $id;
  private $nom;
  private $email;
  private $password;
  private $isAdmin;
  private $isActif;
  private $token;

  public function __construct() {}

  public static function createUser($nom, $email, $password, $isAdmin, $isActif, $token) {
    $user = new self();
    $user->setNom($nom);
    $user->setEmail($email);
    $user->setPassword($password);
    $user->setIsAdmin($isAdmin);
    $user->setIsActif($isActif);
    $user->setToken($token);
    return $user;
  }

  public function getId() { return $this->id; }
  public function setId($id) { $this->id = $id; }

  public function getNom() { return $this->nom; }
  public function setNom($nom) { $this->nom = $nom; }

  public function getEmail() { return $this->email; }
  public function setEmail($email) { $this->email = $email; }

  public function getPassword() { return $this->password; }
  public function setPassword($password) { $this->password = $password; }

  public function getIsAdmin() { return $this->isAdmin; }
  public function setIsAdmin($isAdmin) { $this->isAdmin = $isAdmin; }

  public function getIsActif() { return $this->isActif; }
  public function setIsActif($isActif) { $this->isActif = $isActif; }

  public function getToken() { return $this->token; }
  public function setToken($token) { $this->token = $token; }

}
?>
