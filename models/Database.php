<?php

include_once('Seance.php');
include_once('User.php');

class Database {

  private $connexion;

  const DB_HOST = "mariadb";
  const DB_PORT = "3306";
  const DB_NAME = "PLO";
  const DB_USER = "adminPLO";
  const DB_PASSWORD = "fB2USnnj";

  public function __construct() {
    try {
      $this->connexion = new PDO(
        "mysql:host=".self::DB_HOST.";
        port=".self::DB_PORT.";
        dbname=".self::DB_NAME.";
        charset=UTF8",
        self::DB_USER,
        self::DB_PASSWORD
      );
    } catch(PDOException $e) {
      echo "Erreur: ".$e->getMessage()."<br/>";
      echo "N°: ".$e->getCode();
    }
  }


/**
* @param{Seance} la séance à sauvegarder
* @return{integer, boolean} retour Id si la séance a été crée ou false sinon
*/

  public function createSeance(Seance $seance) {
    $pdoStatement = $this->connexion->prepare(
      "INSERT INTO seances(titre, description, heureDebut, date, duree, nbParticipantsMax, couleur)
      VALUES (:titre, :description, :heureDebut, :date, :duree, :nbParticipantsMax, :couleur)"
    );

    $pdoStatement->execute([
      "titre"               =>  $seance->getTitre(),
      "description"         =>  $seance->getDescription(),
      "heureDebut"          =>  $seance->getHeureDebut(),
      "date"                =>  $seance->getDate(),
      "duree"               =>  $seance->getDuree(),
      "nbParticipantsMax"   =>  $seance->getNbParticipantsMax(),
      "couleur"             =>  $seance->getCouleur()
    ]);

    if($pdoStatement->errorCode() == 0) {
      $id = $this->connexion->lastInsertId();
      return $id;
    }else{
      return false;
    }
  }


/**
*@param{User}
*@return{integer, boolean}
*/
  public function createUser(User $user) {
    $pdoStatement = $this->connexion->prepare(
      "INSERT INTO users(nom, email, password, isAdmin, isActif, token)
      VALUES (:nom, :email, :password, :isAdmin, :isActif, :token)"
    );

    $pdoStatement->execute([
      "nom"       =>  $user->getNom(),
      "email"     =>  $user->getEmail(),
      "password"  =>  $user->getPassword(),
      "isAdmin"   =>  $user->getIsAdmin(),
      "isActif"   =>  $user->getIsActif(),
      "token"     =>  $user->getToken()
    ]);

    if($pdoStatement->errorCode() == 0){
      $id = $this->connexion->lastInsertId();
      return $id;
    }else{
      return false;
    }
  }


/**
*@param{integer} id de la séance recherchée
*@return{Seance|boolean} retourne un objet Seance si la séance a été trouvée sinon il renvoit false
*/
  public function getSeanceById($id) {

    $pdoStatement = $this->connexion->prepare(
      "SELECT * FROM seances WHERE id = :id"
    );
    $pdoStatement->execute(
      ["id" => $id]
    );

    $seance = $pdoStatement->fetchObject("Seance");
    return $seance;
  }

/**
*@param{integer}
*@return{User|boolean}
*/
public function getUserById($id){
  $pdoStatement = $this->connexion->prepare(
    "SELECT * FROM users WHERE id = :id"
  );
  $pdoStatement->execute(
    ["id" => $id]
  );

  $user = $pdoStatement->fetchObject("User");
  return $user;
}

/**
*@param{string}
*@return{User|boolean}
*/
  public function getUserByEmail($email) {
    $pdoStatement = $this->connexion->prepare(
      "SELECT * FROM users WHERE email = :email"
    );
    $pdoStatement->execute(
      ["email"  => $email]
    );

    $user = $pdoStatement->fetchObject("User");
    return $user;
  }


/**
*@param{integer}
*@return{boolean}
*/
  public function activateUser($id){
    $pdoStatement = $this->connexion->prepare(
      "UPDATE users
      SET isActif = 1
      WHERE id = :id"
    );
    $pdoStatement->execute(
      ["id" => $id ]
    );

    if($pdoStatement->errorCode() == 0){
      return true;
    }else{
      return false;
    }
  }

/**
*@param{string}
*@return{boolean}
*/
  public function isEmailExists($email) {
    $pdoStatement = $this->connexion->prepare(
      "SELECT COUNT(*) FROM users WHERE email = :email"
    );
    $pdoStatement->execute(
      ["email"  => $email]
    );

    $nbUser = $pdoStatement->fetchColumn();
    if($nbUser == 0) {
      return false;
    }else{
      return true;
    }
  }



/**
*@param{integer} id de la séance recherchée
*@return{array} retourne un tableau de séances
*/

  public function getSeanceByWeek($week) {

    $pdoStatement = $this->connexion->prepare(
      "SELECT * FROM seances WHERE WEEKOFYEAR(date) = :week
      ORDER BY date, heureDebut"
    );
    $pdoStatement->execute(
      ["week" => $week]
    );

    $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Seance");
    return $seances;
  }


/**
*@param{integer}
*@return{array}
*/
  public function getSeanceByUserId($idUser) {
    $pdoStatement = $this->connexion->prepare(
      "SELECT s.* FROM seances s INNER JOIN inscrits i
      ON s.id = i.id_seance
      WHERE i.id_user = :id_user"
    );
    $pdoStatement->execute(
      ["id_user"  => $idUser]
    );

    $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Seance");
    return $seances;


  }



/**
*@param{Seance} id de la séance recherchée
*@return{boolean} true si la séance est mise à jours ou false sinon
*/

  public function updateSeance(Seance $seance){

    $pdoStatement = $this->connexion->prepare(
      "UPDATE seances SET
      titre = :titre,
      description = :description,
      heureDebut = :heureDebut,
      date = :date,
      duree = :duree,
      nbParticipantsMax = :nbParticipantsMax,
      couleur = :couleur
      WHERE id = :id"
    );

    $pdoStatement->execute(
      [
        "titre"               =>  $seance->getTitre(),
        "description"         =>  $seance->getDescription(),
        "heureDebut"          =>  $seance->getHeureDebut(),
        "date"                =>  $seance->getDate(),
        "duree"               =>  $seance->getDuree(),
        "nbParticipantsMax"   =>  $seance->getNbParticipantsMax(),
        "couleur"             =>  $seance->getCouleur(),
        "id"                  =>  $seance->getId()
      ]
    );

    if($pdoStatement->errorCode() == 0) {
      return true;
    }else{
      return false;
    }

  }


/**
*@param{integer} id de la séance recherchée
*@return{Seance|boolean} retourne un objet Seance si la séance a été trouvée sinon il renvoit false
*/

  public function deleteSeance($id){
    $pdoStatement = $this->connexion->prepare(
      "DELETE FROM inscrits WHERE id_seance = :id_seance"
    );
    $pdoStatement->execute(
      ["id_seance" => $id]
    );

    if($pdoStatement->errorCode() != 0){
      return false;
    }


    $pdoStatement = $this->connexion->prepare(
      "DELETE FROM seances WHERE id = :seance"
    );
    $pdoStatement->execute(
      ["seance" => $id]
    );

    if($pdoStatement->errorCode() == 0){
      return true;
    }else{
      return false;
    }

  }





  public function insertParticipant($idUser, $idSeance){
    $pdoStatement = $this->connexion->prepare(
      "INSERT INTO inscrits(id_user, id_seance)
      VALUES (:id_user, :id_seance)"
    );

    $pdoStatement->execute(
      [ "id_user"   => $idUser,
        "id_seance" => $idSeance ]
    );

    if($pdoStatement->errorCode() == 0){
      return true;
    }else{
      return false;
    }
  }

  public function deleteParticipant($idUser, $idSeance){
    $pdoStatement = $this->connexion->prepare(
      "DELETE FROM inscrits WHERE id_user = :id_user AND id_seance = :id_seance"
    );

    $pdoStatement->execute(
      [ "id_user"   => $idUser,
        "id_seance" => $idSeance ]
    );

    if($pdoStatement->errorCode() == 0){
      return true;
    }else{
      return false;
    }
  }






  public function deleteAllSeances(){
    $pdoStatement = $this->connexion->prepare(
      "DELETE FROM seances;"
    );
    $pdoStatement->execute();
  }

  public function deleteAllUsers(){
    $pdoStatement = $this->connexion->prepare(
      "DELETE FROM users;"
    );
    $pdoStatement->execute();
  }

  public function deleteAllInscrits(){
    $pdoStatement = $this->connexion->prepare(
      "DELETE FROM inscrits;"
    );
    $pdoStatement->execute();
  }
}
?>
