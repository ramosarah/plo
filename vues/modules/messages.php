<?php
if (isset($_SESSION["error"])){
?>

  <div class="alert alert-danger" role="alert">
    <strong><?php echo $_SESSION["error"]; ?></strong>
  </div>

<?php
  $_SESSION["error"] = null;
}
?>



<?php
if(isset($_SESSION["info"])) {
?>

  <div class="alert alert-succes" role="alert">
    <strong><?php echo $_SESSION["info"]; ?></strong>
  </div>

<?php
  $_SESSION["info"] = null;
}
