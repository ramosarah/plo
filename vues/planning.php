<?php
  include('modules/partie1.php');
?>

<?php
require_once(__DIR__."/../models/Database.php");
$database = new Database();

const LUNDI = 1;
const MARDI = 2;
const MERCREDI = 3;
const JEUDI = 4;
const VENDREDI = 5;
const SAMEDI = 6;

$seances = [];
$seances[LUNDI] = [];
$seances[MARDI] = [];
$seances[MERCREDI] = [];
$seances[JEUDI] = [];
$seances[VENDREDI] = [];
$seances[SAMEDI] = [];

$weekNumber = date("W");
$seancesOfWeek = $database->getSeanceByWeek($weekNumber);

foreach ($seancesOfWeek as $seance) {
  $indexDay = date("w", strtotime($seance->getDate()));
  array_push($seances[$indexDay], $seance);
}

?>

<div class="container card text-center mt-4">
  <h1 class="card-header">Planning de la semaine</h1>
  <div class="card-body">
    <div class="row">

      <div class="col-6 col-md-4 col-lg-2 border border-primary border-top-0" id="lundi">
        <h3>LUNDI</h3>
        <?php
          foreach ($seances[LUNDI] as $seance) {
            include('modules/etiquette.php');
          }
        ?>
      </div>

      <div class="col-6 col-md-4 col-lg-2 border border-primary border-top-0" id="jeudi">
        <h3>MARDI</h3>
        <?php
          foreach ($seances[MARDI] as $seance) {
            include('modules/etiquette.php');
          }
        ?>
      </div>


      <div class="col-6 col-md-4 col-lg-2 border border-primary border-top-0" id="mercredi">
        <h3>MERCREDI</h3>
        <?php
          foreach ($seances[MERCREDI] as $seance) {
            include('modules/etiquette.php');
          }
        ?>
      </div>

      <div class="col-6 col-md-4 col-lg-2 border border-primary border-top-0" id="jeudi">
        <h3>JEUDI</h3>
        <?php
          foreach ($seances[JEUDI] as $seance) {
            include('modules/etiquette.php');
          }
        ?>
      </div>

      <div class="col-6 col-md-4 col-lg-2 border border-primary border-top-0" id="vendredi">
        <h3>VENDREDI</h3>
        <?php
          foreach ($seances[VENDREDI] as $seance) {
            include('modules/etiquette.php');
          }
        ?>
      </div>
    </div>
  </div>
  <a class="btn btn-dark" href="index.php">Revenir à la page d'accueil</a>

</div>


<?php
  include('modules/partie3.php');
?>
